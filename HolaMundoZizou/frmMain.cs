using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HolaMundoZizou
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Hola Mundo de PCP 1-2";
            label1.BackColor = Color.Chocolate;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "Adiós Mundo de PCP 1-2";
            label1.BackColor = Color.Crimson;
        }
    }
}
